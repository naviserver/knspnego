# Copyright 2007-2008 KnowNow, Inc.
#
# @KNOWNOW_LICENSE_START@
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in
# the documentation and/or other materials provided with the
# distribution.
#
# 3. The name "KnowNow" is a trademark of KnowNow, Inc. and may not
# be used to endorse or promote any product without prior written
# permission from KnowNow, Inc.
#
# THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESSED OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL KNOWNOW, INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
# GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
# IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# @KNOWNOW_LICENSE_END@
#
# To use this, you register the filter using ns_registerfilter, setting
# it up as a preauth filter. This can't be used as-is; it relies on
# KnowNow's "knconn" utilities, which hacked in a solution for setting user
# ids from authentication modules.
#
# 
ns_log Notice "kn_spnego(tcl):KNSPNEGO FINAL Started "
# _kn_spnego_filter
#
# AOLServer HTTP filter for hooking in session recognition.
#
# The argument communicates the part of the URL space that users
# are allowed access to without any credentials (typically the login
# tree).  If null, then either session cookies or basic authentication must be 
# provided in every request.
#
# The argument is applied as a [string match $arg $localurl]; the 
# argument is provided in the registration lines at the bottom of this file.
# 
#AMX: Here we'll parse request Headers again, although it has been already done in
#     the ConnRun (Conn*) function. But since neither NS_Conn struct nor Conn struct
#     has appropriate field for putting auth. type, we have to do it here again,
#     
proc _kn_spnego_filter {conn arg context} {
      ns_log Notice "kn_spnego_filter: 1"
      if {![kn_conn isconnected]} {
			# Not much that can be done; must be an internal operation, so we'll
			# let it pass through
        return filter_ok;
        }
     set hdrs [kn_conn get headers]
     #AMX: Initial request 
     #amx:
     set spnego_hdr [ns_set get $hdrs Authorization]
     ns_log Notice "kn_spnego_filter:1.1 - Header is $spnego_hdr"
     if {"" == $spnego_hdr} {
      ns_log Notice "kn_spnego_filter:1.2 - Header is $spnego_hdr"
      # set auser [kn_conn get user]
       set vuser [kn_conn get validuser]
       ns_log Notice "kn_spnego_filter:1.2 - Validuser is $vuser"
       if {"" == $vuser } {
          set auth_mech Negotiate
			    kn_conn set outputheader WWW-Authenticate $auth_mech
			    ns_log Notice "kn_spnego_filter: 2 Unathorized: set auth_mech to $auth_mech"
			    #kn_trace "kn_spnego_filter:  Unathorized: set outputheader to '$outputheader'"
			    #AMX: would be nice to call here the ns_returnunauthorized. But currently it
			    # handles only the Basic auth. mechanism. So we'll take care about respond  
			    #ns_returnunauthorized
			    ns_respond -status 401 -string "HTTP/1.0 401 Unauthorized"
			    ns_log Notice "kn_spnego_filter: 3 - return_filter"
			    return filter_return;
			    # #conn with valid user
       } elseif {"" !=$vuser} {
         kn_trace "kn_spnego_filter: the connection already has a valid user - '$vuser'"   
         #auth. ok continue with authorization
         ns_log Notice "kn_spnego_filter: 4"
         return filter_break  
       }
      #the header contains Authorization key word
     } elseif {"" != $spnego_hdr} {  
        ns_log Notice "kn_spnego_filter: 5 header is $spnego_hdr"
        #the Auth. mechanis is negotiate
        if {"Negotiate" == [set auth_mech [lindex [split $spnego_hdr \ ] 0]]} {
         ns_log Notice "kn_spnego_filter: 6 auth_mechis $auth_mech"
         # move decoding to the knspnego 
         set token [lindex [split $spnego_hdr \ ] 1]
         #Calling SPNEGO module; if successfull conn will have a valid user name   
         kn_trace "kn_spnego_filter: Calling kn_spnego auth-module "
         ns_log Notice "kn_spnego_filter: 7 Calling knspnego auth-module:TOKEN IS $token"
         #set auser [kn_spnego conn $token]
         set auser [kn_spnego $token]
         set userid [lindex [split $auser @ ] 0]
         ns_log notice "kn_spnego_filter: 6.0 The userid Is '$userid'"

		kn_conn set validuser $userid
		#kn_conn set validuser [lindex [split $auser @ ] 0]
        # ns_log Notice "kn_spnego_filter: 6.1 After Authentication is The User is '$validuser'"
         ns_log Notice "kn_spnego_filter: 6.2 After Authentication is The User is '$auser'"
         if { "" != $auser } {
            #AMX: the user and domain has been injected in conn structure in knspnego
            kn_trace "kn_spnego_filter: Authentication is succesfull. The User is '$auser'" 
            # auth. ok continue with authorization
            ns_log Notice "kn_spnego_filter: 7 Authentication is succesfull. The User is '$auser'"
           # ns_return -status "200 OK" -type text/plain -string "Session $sessiontopic is still active"
         #  ns_respond -status "200" -type text/plain -string "OK"
           #return filter_ok;  
           return filter_break;  
         } else {
          #SPMEGO:  auth failed: no valid credentials were provided or may be sec. context not complete RFC 2274
           kn_conn set outputheader WWW-Authenticate $auth_mech
		    # kn_trace "kn_spnego_filter: Unathorized - No valid User: set outputheader to '$outputheader'"
           #ns_log Notice "kn_spnego_filter: 8 Unathorized - No valid User: set outputheader to $auth_mech"
           ns_respond -status 401 -string "HTTP/1.0 401 Unauthorized"
           return filter_return;
         }
        } else {
				#AMX: different Authentication mechanism is supported by the client
				 kn_trace "kn_spnego_filter: Client supports '$auth_mech' Authentication mechanism"
				 ns_log Notice "kn_spnego_filter: 9 Client supports '$auth_mech' Authentication mechanism"
				 # It is not Negotiate PROTOCOL- continue filter processing
				 return filter_ok;
			  }
       }
       return filter_ok;
     }
