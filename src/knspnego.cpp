/*
 * (c) Copyright 2008 KnowNow, Inc., Sunnyvale CA
 *
 * @KNOWNOW_LICENSE_START@
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in
 * the documentation and/or other materials provided with the
 * distribution.
 *
 * 3. The name "KnowNow" is a trademark of KnowNow, Inc. and may not
 * be used to endorse or promote any product without prior written
 * permission from KnowNow, Inc.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL KNOWNOW, INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @KNOWNOW_LICENSE_END@
* 
*/
#include "knexportlibraryknspnegomodule.h"
#include "ns.h" 
#include "spnegoconfig.h"
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <time.h>


#define bzero(dst,len) memset(dst,0,len)
#define bcopy(src,dst,len) memcpy(dst,src,len)


extern "C" 
{
	NS_EXPORT int Ns_ModuleVersion = 1;
	EXPORT_LIBRARY_KNSPNEGO int Ns_ModuleInit(const char *server, const char *module);
}


#define b64_sizeof(x) (((sizeof(x) + 2) / 3) * 4)
#define b64_size(x)   (((x + 2) / 3) * 4)

#define BADARGS(nl,nh,example) \
	if ((argc<(nl)) || (argc>(nh))) { \
		Tcl_AppendResult(interp,"wrong # args: should be \"",argv[0], \
			(example),"\"",NULL); \
			return NS_ERROR; \
//	}

#define ENC_HEADER_LEN        128
#define MIN_ENCODED_STR_LEN    59

int authenticateUser (char** authUser, const char *authToken);
SPNEGO_CONFIG *spConfig = NULL;
static Ns_Callback ModuleCleanup;
static Ns_TclTraceProc spnegoInterpInit;
static Tcl_CmdProc kn_spnego;
 
/*
 *----------------------------------------------------------------------
 *
 * KnSpnego_ModInit --
 *
 *      This is the knspnego module's entry point.  AOLserver runs
 *      this function right after the module is loaded.  It is used to
 *      read configuration data, initialize data structures, kick off
 *      the Tcl initialization function (if any), and do other things
 *      at startup.
 *
 * Results:
 *	NS_OK or NS_ERROR
 *
 *----------------------------------------------------------------------
 */
EXPORT_LIBRARY_KNSPNEGO int
Ns_ModuleInit(const char *hServer, const char * hModule)
{
    FILE *fd = NULL;
    Ns_DString ds;
    char *publickeyfile = NULL;
    char *privatekeyfile = NULL;
    const char *path;
    
    Ns_Log(Notice, "KN_SPNEGO_*****_ nit(): serv = %s module = %s", hServer, hModule );
    Ns_DStringInit(&ds);
    
	SPNEGO_CONFIG *spConfig = (SPNEGO_CONFIG *)malloc(sizeof(SPNEGO_CONFIG));
	spConfig->directoryConfig = (DIRECTORY_CONFIG *)malloc(sizeof(DIRECTORY_CONFIG));
    spConfig->serverConfig = (SERVER_CONFIG *)malloc(sizeof(SERVER_CONFIG));
    path = Ns_ConfigGetPath(hServer, hModule, NULL);
  	
    if (path) {
        spConfig->directoryConfig->setKrb5ServiceName((char *)Ns_ConfigGetValue(path, "Krb5ServiceName"));
        spConfig->directoryConfig->setKrb5KeyTabFile((char *)Ns_ConfigGetValue(path, "Krb5KeyTabFile"));
        spConfig->setAuthType((char *)Ns_ConfigGetValue(path, "AuthType"));
    }
    return Ns_TclRegisterTrace(hServer, spnegoInterpInit, NULL, NS_TCL_TRACE_CREATE);
}

/*
 *----------------------------------------------------------------------
 *
 * ModuleCleanup --
 *
 *      Deallocates allocated resources.
 *
 * Results:
 *	None
 *
 *----------------------------------------------------------------------
 */

static void ModuleCleanup(void *ignored)
{
}

/*
 *----------------------------------------------------------------------
 *
 * spnegoInterpInit --
 *
 *      Register new commands with the Tcl interpreter.
 *
 * Results:
 *	NS_OK or NS_ERROR
 *
 *----------------------------------------------------------------------
 */

static Ns_ReturnCode spnegoInterpInit(Tcl_Interp * interp, const void *context)
{
    Tcl_CreateCommand(interp, "kn_spnego", kn_spnego, NULL, NULL);
    return NS_OK;
}



/*
 *----------------------------------------------------------------------
 *
 * kn_spnego --
 *
 *      Gets the authentication token and calls the authenticateUser() 
 *		function. Send the authentication results back to the tcl filter.
 *
 * Results:
 *	NS_OK
 *
 *----------------------------------------------------------------------
 */

static int kn_spnego(ClientData context, Tcl_Interp * interp, int argc, CONST84 char *argv[])
{
    FILE *fd = NULL;
    int keybits = 0;
    int firstarg = 1;
    CONST84 char *token = NULL;
	Ns_Log(Notice, "KN_SPNEGO_**********_ kn_spneg(): argsnmb = %d", argc );
//	DebugBreak();

	    token = argv[firstarg];
		char *authUser =NULL;
		authenticateUser (/*connPtr,*/&authUser, (const char *)token);
		Ns_Log(Notice, "knspnego - ***AUTHENTICATED USER Before sending IS %s", authUser);     
		Tcl_AppendResult(interp, authUser, NULL);
		if(authUser)
		{
		 free(authUser);
		}
	return TCL_OK;
}


/*
 * Local Variables:
 * mode: c
 * c-basic-offset: 4
 * fill-column: 78
 * indent-tabs-mode: nil
 * End:
 */
