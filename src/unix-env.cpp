/* unix-env.c */

/*
Frank Balluffi modified the following code from Apache 2.0.
*/

/* ====================================================================
 * The Apache Software License, Version 1.1
 *
 * Copyright (c) 2000-2003 The Apache Software Foundation.  All rights
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:
 *       "This product includes software developed by the
 *        Apache Software Foundation (http://www.apache.org/)."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "Apache" and "Apache Software Foundation" must
 *    not be used to endorse or promote products derived from this
 *    software without prior written permission. For written
 *    permission, please contact apache@apache.org.
 *
 * 5. Products derived from this software may not be called "Apache",
 *    nor may "Apache" appear in their name, without prior written
 *    permission of the Apache Software Foundation.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE APACHE SOFTWARE FOUNDATION OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 */

//#ifdef APACHE13
// #include "httpd.h"

//#define APR_DECLARE(rc) rc
//#define APR_ENOMEM      1
//#define APR_ENOTIMPL    1
//#define APR_SUCCESS     0
//#define apr_palloc      ap_palloc
//#define apr_pool_t      pool
#ifndef _WINDOWS
#define HAVE_SETENV

typedef int    apr_status_t;
//typedef size_t apr_size_t;

//APR_DECLARE(apr_status_t) apr_env_set(const char *envvar,
//                                     const char *value//, 
//                                    /*apr_pool_t *pool*/)
#include "spnegoconfig.h"

int apr_env_set(const char *envvar,
                const char *value)
{
#if defined(HAVE_SETENV)

    if (0 > setenv(envvar, value, 1))
        return 1; // APR_ENOMEM;
    return 0; //APR_SUCCESS;

#elif defined(HAVE_PUTENV)

    apr_size_t elen = strlen(envvar);
    apr_size_t vlen = strlen(value);
    char *env = apr_palloc(pool, elen + vlen + 2);
    char *p = env + elen;

    memcpy(env, envvar, elen);
    *p++ = '=';
    memcpy(p, value, vlen);
    p[vlen] = '\0';

    if (0 > putenv(env))
        return APR_ENOMEM;
    return APR_SUCCESS;

#else
    return APR_ENOTIMPL;
#endif
}
#endif
