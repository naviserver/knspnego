/* -----------------------------------------------------------------------------
 * mod_spnego.cpp  - is modified version of mod_spnego.c to support 
 *  authentication for AOLServer, via the RFC 2478 SPNEGO GSS-API mechanism.
 *
 *  Original mod_spnego.c was supporting GSS-API authentication mechanism for
 *  Apache web server
 *
 *  Assumes use from a TCL-based pre-auth filter.
 * 
 * Author: Avet Mnatsakanian
 * Authors of original module: Frank Balluffi and Markus Moeller
 *
 */

/* Copyright 2000-2005 The Apache Software Foundation or its licensors, as
 * applicable.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include "ns.h"
#include "string.h"

#undef strcasecmp
#undef strncasecmp

#ifdef HEIMDAL
#include <gssapi.h>
#define gss_nt_service_name GSS_C_NT_HOSTBASED_SERVICE
#elif defined(SEAM)
#include <gssapi/gssapi.h>
#define gss_nt_service_name GSS_C_NT_HOSTBASED_SERVICE
#else
#include <gssapi_generic.h>
#endif
#include "spnegohelp.h"
#include "krb5help.h"
#include "spnegoconfig.h"

#include "base64.h"
#include <errno.h>

static char * connectionUser;
DIRECTORY_CONFIG* SPNEGO_CONFIG::directoryConfig = NULL;
SERVER_CONFIG* SPNEGO_CONFIG::serverConfig = NULL;
const char *SPNEGO_CONFIG::authType=NULL;

static int handleKerberosToken (char **authUser,
                                const unsigned char * inputKerberosToken,
                                size_t                inputKerberosTokenLength,
                                unsigned char **      outputKerberosToken,
                                size_t *              outputKerberosTokenLength)
{
    gss_buffer_desc    buffer          = GSS_C_EMPTY_BUFFER;
    gss_buffer_desc    serviceBuffer   = GSS_C_EMPTY_BUFFER;
    gss_name_t         clientName      = GSS_C_NO_NAME;
    gss_ctx_id_t       context         = GSS_C_NO_CONTEXT;
    gss_cred_id_t      credential      = GSS_C_NO_CREDENTIAL;
    gss_buffer_desc    inputToken      = GSS_C_EMPTY_BUFFER;
    OM_uint32          majorStatus;
    OM_uint32          minorStatus1;
    OM_uint32          minorStatus2;
    gss_buffer_desc    outputToken     = GSS_C_EMPTY_BUFFER;
    int                rc              = TCL_OK;
    gss_name_t         serverName      = GSS_C_NO_NAME;
    char             * ServiceName     = NULL;
    char             * Krb5ServiceName = NULL;
    const char	     * Krb5KeyTabFile  = NULL;

    errno = 0;
    Krb5ServiceName = (char *)(SPNEGO_CONFIG::getDirConfig())->getKrb5ServiceName();
    ServiceName = strtok(Krb5ServiceName," ");
    
    while ( ServiceName != NULL )  {
        serviceBuffer.value = strdup(ServiceName);       
        serviceBuffer.length = sizeof (serviceBuffer.value) ;
        Ns_Log(Notice, "mod_SPNEGO - handleKerberosToken_1 ServiceName=%s",ServiceName);
        
        if (strchr(ServiceName,'/')) {
	    Ns_Log(Notice, "mod_SPNEGO - handleKerberosToken_2 ");
            majorStatus = gss_import_name (&minorStatus1,
                                           &serviceBuffer, 
                                           (gss_OID) GSS_C_NULL_OID,
                                           &serverName);
        } else {
            Ns_Log(Notice, "mod_SPNEGO - handleKerberosToken_3 gss_nt_service_name=%s",(char *)gss_nt_service_name);
            Ns_Log(Notice, "mod_SPNEGO - handleKerberosToken_3 gss_nt_service_name=%s",(char *)&serverName);
            majorStatus = gss_import_name (&minorStatus1,
                                           &serviceBuffer, 
                                           (gss_OID) gss_nt_service_name,
                                           &serverName);
        }
        ServiceName = strtok(NULL," ");
	if (majorStatus != GSS_S_COMPLETE) {
            Ns_Log(Notice, "mod_SPNEGO - handleKerberosToken_4;GSS_S_COMPLETE - not");           
            return  HTTP_INTERNAL_SERVER_ERROR;
        }
        
        /* Optionally, set environment variable KRB5_KTNAME. */
        Krb5KeyTabFile = (const char *)(SPNEGO_CONFIG::getDirConfig())->getKrb5KeyTabFile();
        //#ifdef _WINDOWS   
        if (Krb5KeyTabFile) {
            if(apr_env_set("KRB5_KTNAME", Krb5KeyTabFile ) != APR_SUCCESS) {
                Ns_Log(Error, "mod_SPNEGO_*******handleKerberosToken unable to set KRB5_KTNAME to %s", Krb5KeyTabFile);
            } else {
#ifdef HEIMDAL
        	gsskrb5_register_acceptor_identity(Krb5KeyTabFile);
#endif
                Ns_Log(Notice, "mod_SPNEGO_*******handleKerberosToken set KRB5_KTNAME to %s", Krb5KeyTabFile);
            }
        }
        //#endif 
        majorStatus = gss_acquire_cred (&minorStatus1,
                                        serverName,
                                        0,
                                        GSS_C_NULL_OID_SET,
                                        GSS_C_ACCEPT,
                                        &credential,
                                        NULL,
                                        NULL);

        if (serverName != GSS_C_NO_NAME)
            {
                Ns_Log(Notice, "mod_SPNEGO - handleKerberosToken_5;GSS_C_NO_NAME - not");
                gss_release_name (&minorStatus2, &serverName);
                serverName = GSS_C_NO_NAME;
            }
        Ns_Log(Notice, "mod_SPNEGO - handleKerberosToken_1xx majorStatus_acquire_cred=%u",majorStatus);
        if (majorStatus != GSS_S_COMPLETE)
            {
                // logGssApiError (APLOG_MARK, APLOG_ERR, r, "mod_spnego: gss_acquire_cred failed", majorStatus, minorStatus1);
                Ns_Log(Notice, "mod_SPNEGO - handleKerberosToken_6;GSS_S_COMPLETE - not");                      
                rc = HTTP_INTERNAL_SERVER_ERROR;
                continue;
            }
        /*
          Normally, gss_accept_sec_context returns GSS_S_CONTINUE_NEEDED if it needs
          to be called again. In this context, gss_accept_sec_context should not
          return GSS_S_CONTINUE_NEEDED. If gss_accept_sec_context returns
          GSS_S_CONTINUE_NEEDED, the decision to release clientName is based on it's
          value (GSS_C_NO_NAME or not GSS_C_NO_NAME) and outputToken is delegated to
          gss_release_buffer -- RFC 2744 is not clear about this case.
        */

        inputToken.value = (unsigned char *) inputKerberosToken;
        inputToken.length = inputKerberosTokenLength;

        majorStatus = gss_accept_sec_context (&minorStatus1,
                                              &context,
                                              credential,
                                              &inputToken,
                                              GSS_C_NO_CHANNEL_BINDINGS,
                                              &clientName,
                                              NULL,
                                              &outputToken,
                                              NULL,
                                              NULL,
                                              NULL);

        if (credential != GSS_C_NO_CREDENTIAL)
            {
                Ns_Log(Notice, "mod_SPNEGO - handleKerberosToken_7 GSS_C_NO_CREDENTIAL");
                gss_release_cred (&minorStatus2, &credential);
                credential = GSS_C_NO_CREDENTIAL;
            }

        if (context != GSS_C_NO_CONTEXT)
            {
                Ns_Log(Notice, "mod_SPNEGO - handleKerberosToken_8 GSS_C_NO_CONTEXT");
                gss_delete_sec_context (&minorStatus2, &context, GSS_C_NO_BUFFER);
                context = GSS_C_NO_CONTEXT;
            }

        if (majorStatus != GSS_S_COMPLETE)
            {
                // logGssApiError (APLOG_MARK, APLOG_ERR, r, "mod_spnego: gss_accept_sec_context failed", majorStatus, minorStatus1);
                Ns_Log(Notice, "mod_SPNEGO - handleKerberosToken_9 GSS_C_NO_CONTEXT");
                rc = HTTP_INTERNAL_SERVER_ERROR;
                continue;
            }

        /* Pass client (or user) name to authorization hook. */
        Ns_Log(Notice, "mod_SPNEGO - handleKerberosToken buffe_10r is %s", (char*)buffer.value);
        majorStatus = gss_display_name (&minorStatus1, clientName, &buffer, NULL);

        if (majorStatus != GSS_S_COMPLETE)
            {
                //            logGssApiError (APLOG_MARK, APLOG_ERR, r, "mod_spnego: gss_display_name failed", majorStatus, minorStatus1);
                rc = HTTP_INTERNAL_SERVER_ERROR;
                goto cleanup;
            }

        /*
          http://httpd.apache.org/docs/misc/API.html says, "You can also see how some
          bugs have manifested themself, such as setting connection->user to a value
          from r->pool -- in this case connection exists for the lifetime of ptrans,
          which is longer than r->pool (especially if r->pool is a subrequest!). So
          the correct thing to do is to allocate from connection->pool." Apache 1.3
          stores user in connection substructure of request_rec. Apache 2.0 stores
          user in request_rec.
        */
        //		connPtr->authUser = ns_strdup((const char*)buffer.value);
	*authUser = strdup((const char*)buffer.value);
        Ns_Log(Notice, "mod_SPNEGO_*******AUTHENTICATED USER IS %s", *authUser);     
	gss_release_buffer (&minorStatus1, &buffer);

        if (outputToken.length)
            {
                *outputKerberosToken = (unsigned char*)malloc (outputToken.length);
                if (!*outputKerberosToken)
                    {
                        rc = HTTP_INTERNAL_SERVER_ERROR;
                        goto cleanup;
                    }
                memcpy (*outputKerberosToken, outputToken.value, outputToken.length);
                *outputKerberosTokenLength = outputToken.length;
            }
        rc = APR_SUCCESS;// rc = OK;
        goto cleanup;
    }
    free(serviceBuffer.value);
 cleanup:

    if (clientName != GSS_C_NO_NAME)
        {
            gss_release_name (&minorStatus1, &clientName);
            clientName = GSS_C_NO_NAME;
        }

    gss_release_buffer (&minorStatus1, &outputToken);
    return rc;
}

/* -----------------------------------------------------------------------------
 * handleSpnegoToken handles an RFC 2478 SPNEGO GSS-API token.
 *
 * If handleSpnegoToken is successful, call free (outputSpnegoToken), where
 * outputSpnegoToken is of type unsigned char *, to free the memory allocated by
 * handleSpnegoToken.
 *
 * Returns an Apache response code.
 * -----------------------------------------------------------------------------
 */

static int handleSpnegoToken (/*Conn  *connPtr,*/
                              char** authUser,
                              const unsigned char * inputSpnegoToken,
                              size_t                inputSpnegoTokenLength,
                              unsigned char **      outputSpnegoToken,
                              size_t *              outputSpnegoTokenLength)
{
    /* Patch by Frank Taylor */
    int             brokenOID                 = 0;
    unsigned char * inputKerberosToken        = NULL;
    size_t          inputKerberosTokenLength  = 0;
    long            negResult;
    unsigned char * outputKerberosToken       = NULL;
    size_t          outputKerberosTokenLength = 0;
    int             rc;

    errno = 0;
#ifdef _DEBUG
    DebugBreak();
#endif
    if (!parseSpnegoInitialToken (inputSpnegoToken,
                                  inputSpnegoTokenLength,
                                  &krb5GssApi,
                                  &inputKerberosToken,
                                  &inputKerberosTokenLength))
        {
            Ns_Log(Notice, "mod_SPNEGO_**********parseSpnegoInitialToken failed for 1.2.840.113554.1.2.2");
            /*
              The correct mechanism OID does not work, let's just check the broken MS
              one in case this is an old W2K client. - Frank Taylor
            */
            if (inputKerberosToken)
                free ((void *)inputKerberosToken);
            inputKerberosToken = NULL;
            if (!parseSpnegoInitialToken (inputSpnegoToken,
                                          inputSpnegoTokenLength,
                                          &msKrb5GssApiLegacy, 
                                          &inputKerberosToken,
                                          &inputKerberosTokenLength))
                {
                    Ns_Log(Notice, "mod_SPNEGO_**********parseSpnegoInitialToken failed for 1.2.840.48018.1.2.2");
                    /*
                      Ideally, handleKerberosToken should be called by authenticateUser,
                      not by handleSpnegoToken. For more information, see comment in
                      authenticateUser.
                    */
                    rc = handleKerberosToken (authUser,
                                              inputSpnegoToken,
                                              inputSpnegoTokenLength,
                                              outputSpnegoToken,
                                              outputSpnegoTokenLength);
                    Ns_Log(Notice, "mod_SPNEGO_********** After -1 calling handleKerberosToken");
                    if (inputKerberosToken)
                        free ((void *)inputKerberosToken);
                    inputKerberosToken = NULL;
                    return rc;
                }

            //AMX: repl by NS_Log ap_log_rerror (APLOG_MARK, PORTABLE_APLOG_INFO, r, "mod_spnego: parseSpnegoInitialToken succeeded for 1.2.840.48018.1.2.2 -- probably Windows 2000 client");
            brokenOID = 1;
        }

    Ns_Log(Notice, "mod_SPNEGO_********** before -2 calling handleKerberosToken");
    rc = handleKerberosToken (authUser,
                              inputKerberosToken,
                              inputKerberosTokenLength,
                              &outputKerberosToken,
                              &outputKerberosTokenLength);
    Ns_Log(Notice, "mod_SPNEGO_********** After -2 calling handleKerberosToken outputKerberosToken=%s",
           outputKerberosToken);

    if (inputKerberosToken) {
        free ((void *)inputKerberosToken);
    }

    negResult = 0;
    if (!makeSpnegoTargetToken (&negResult,
                                /* Frank Taylor */
                                (brokenOID ? &msKrb5GssApiLegacy : &krb5GssApi),
                                outputKerberosToken,
                                outputKerberosTokenLength,
                                NULL,
                                0,
                                outputSpnegoToken,
                                outputSpnegoTokenLength)) {
        Ns_Log(Notice, "mod_SPNEGO_********** After -3 calling makeSpnegoTargetToken outputSpnegoToken=%s",
               *outputSpnegoToken);
        return HTTP_INTERNAL_SERVER_ERROR;
    }

    return rc;
}

/* -----------------------------------------------------------------------------
 * authenticateUser authenticates a user (or Apache client).
 *
 * Returns an Apache response code.
 * -----------------------------------------------------------------------------
 */

//static int authenticateUser (Conn * conPtr)
int authenticateUser (char **authUser, const char *authToken)
{
    static unsigned char ntlmProtocol [] = {'N', 'T', 'L', 'M', 'S', 'S', 'P', 0};

    const char *    authType          = NULL;
    unsigned char * inputToken        =  NULL;
    size_t          inputTokenLength  =  0u; //sizeof (authToken); //AMX: auth token already decoded in tcl;
    unsigned char * outputToken       = NULL;
    size_t          outputTokenLength = 0;
    int             rc;
    char *          string            = NULL;

    errno = 0;
    
    /* Get AuthType. */
    authType = SPNEGO_CONFIG::getAuthType(); //getting authType from config params
	
    if (!authType || strcasecmp (authType, "SPNEGO"))
        {
            //AMX: repl by NS_Log ap_log_rerror (APLOG_MARK, PORTABLE_APLOG_INFO, r, "mod_spnego: unrecognized AuthType \'%s\'", authType ? authType : "NULL");
            return -1;// DECLINED;
        }

    //AMX: The request for already authenticatedUser (!!?). So just ret OK
    /*AMX TEMP
      if (!(SPNEGO_CONFIG::getServerConfig())->getKrb5AuthEachReq() && connectionUser)
      {
      //AMX: repl by NS_Log ap_log_rerror (APLOG_MARK, PORTABLE_APLOG_INFO, r, "mod_spnego: setting request user to %s", connectionUser);
      connPtr->authUser = ns_strdup(connectionUser);
      return APR_SUCCESS; //OK
      }
    */
    //#endif
    //This part is done in kn_spnego_filer 
    //  authValue = apr_table_get (r->headers_in, "Authorization");
    //AMX-TODO: For now we may drop this function assuming that only kn_spengo_filter
    // is the only caller of autUser func. And the not only authToken (Negotiate + sec_token), 
    // but the token itself is retrieved in tcl and passed as an arg to AuthenticateUser() function
    //So authValue presumably is not zero if the function was called
    /*   if (!authValue)
         {
         //AMX: repl by NS_Log ap_log_rerror (APLOG_MARK, PORTABLE_APLOG_INFO, r, "mod_spnego: sending 401 and \"WWW-Authenticate: Negotiate\"");
         apr_table_add (r->err_headers_out, "WWW-Authenticate", "Negotiate");
         return HTTP_UNAUTHORIZED;
         }
    */
    //AMX  if (!strncasecmp (string, "Negotiate", 9))
    //AMX  {
    //AMX    string = ap_getword_white (r->pool, &authValue);

    //AMX     if (!string)
    //AMX         return HTTP_UNAUTHORIZED;

    //AMX: all decodin is done in kn_spnego_filter by utilizing
    // the base64::decode library function; SO all this part is gone
    /************** */
    //We already got the Token from tcl - so we don't need the above lines */
    //  inputTokenLength = base64_decode_len (string); 
    inputTokenLength = base64_decode_len (authToken);
    inputToken = (unsigned char*)malloc(inputTokenLength);
    //#endif

    if (!inputToken)
        {
            return HTTP_INTERNAL_SERVER_ERROR;
        }

    inputTokenLength = base64_decode ((char *)inputToken, authToken);
    string = NULL;

    /* Check if NTLM token. */
    Ns_Log(Notice, "MOD_SPNEGO_*****_ authorizeUser(): Client respond to Negotiate challenge with %s  auth. mechanism",
           inputToken );
    
    if (inputTokenLength >= sizeof ntlmProtocol + 1) {
        if (!memcmp (inputToken, ntlmProtocol, sizeof ntlmProtocol)) {
            //AMX: repl by NS_Log ap_log_rerror (APLOG_MARK, PORTABLE_APLOG_ERR, r, "mod_spnego: received type %d NTLM token", (int) *(inputToken + sizeof ntlmProtocol));
            Ns_Log(Notice, "MOD_SPNEGO_*****_ authorizeUser(): Client respond to Negotiate challenge with %s  auth. mechanism",
                   inputToken );                
            return HTTP_UNAUTHORIZED;
        }
    }
    
    /*
      Ideally, authenticateUser should parse RFC 2743 InitialContextTokens,
      look at mechType element and call handleSpnegoToken or
      handleKerberosToken. Because d2i_GSSAPI_INITIAL_CONTEXT_TOKEN fails to
      parse RFC 1964 Kerberos tokens (they contain non-DER token IDs after
      mechType element), handleSpnegoToken passes input tokens to
      handleKerberosToken if parseSpnegoInitialToken fails.
    */
    Ns_Log(Notice, "MOD_SPNEGO_*****_ authenticateUser(): Before calling handleSpnegoToken" );                
    rc = handleSpnegoToken (/*connPtr, */authUser,
                            inputToken,
                            inputTokenLength,
                            &outputToken,
                            &outputTokenLength);
    Ns_Log(Notice, "MOD_SPNEGO_*****_ authenticateUser(): After calling handleSpnegoToken" );                
    int len = base64_encode_len ((int) outputTokenLength);
    string = ( char *)malloc(size_t(len));
    if (!string)
        {
            //AMX: repl by NS_Log ap_log_rerror (APLOG_MARK, PORTABLE_APLOG_ERR, r, "mod_spnego: apr_pcalloc failed");
            if (outputToken)
                free (outputToken);
            return HTTP_INTERNAL_SERVER_ERROR;
        }
    Ns_Log(Notice, "MOD_SPNEGO_*****_ authenticateUser(): outputToken = %s", outputToken );                
    base64_encode (string, (const char*)outputToken, (int) outputTokenLength);

    if (outputToken)
        free (outputToken);
    /* Does apr_table_set copy string? */
    //AMX: Is this mistake ? shoulddn't we see the the returning of 200 OK msg
    /*AMX FIND OUT WHY WE SHOULD PUT IN HEADERS
      apr_table_set (r->err_headers_out,
      "WWW-Authenticate",
      apr_pstrcat (r->pool, "Negotiate ", string, NULL));
      //AMX: repl by NS_Log ap_log_rerror (APLOG_MARK, PORTABLE_APLOG_INFO, r, "mod_spnego: authenticateUser returning %d", rc);
      */
    if(string)
        free(string);
    return rc;
}


/* -----------------------------------------------------------------------------
 * logGssApiError writes GSS-API messages to Apache's error log.
 *
 * Returns nothing.
 * -----------------------------------------------------------------------------
 */
/*
  static void logGssApiError (const char *        file,
  int                 line,
  int                 level,
  const request_rec * r,
  char *              s,
  OM_uint32           maj_stat,
  OM_uint32           min_stat)
  {
  OM_uint32       gmaj_stat;
  OM_uint32       gmin_stat;
  gss_buffer_desc msg;
  OM_uint32       msg_ctx = 0;

  while (!msg_ctx)
  {
  gmaj_stat = gss_display_status (&gmin_stat,
  maj_stat,
  GSS_C_GSS_CODE,
  GSS_C_NULL_OID,
  &msg_ctx,
  &msg);

  if (gmaj_stat == GSS_S_COMPLETE)
  {
  #ifdef APACHE13
  //AMX: repl by NS_Log ap_log_rerror (file, line, level, r, "%s; GSS-API: %s)", s, (char *) msg.value);
  #else
  //AMX: repl by NS_Log ap_log_rerror (file, line, level, 0, r, "%s; GSS-API: %s)", s, (char *) msg.value);
  #endif
  gss_release_buffer (&gmin_stat, &msg);
  break;
  }

  gss_release_buffer (&gmin_stat, &msg);
  }

  msg_ctx = 0;

  while (!msg_ctx)
  {
  gmaj_stat = gss_display_status (&gmin_stat,
  min_stat,
  GSS_C_MECH_CODE,
  GSS_C_NULL_OID,
  &msg_ctx,
  &msg);

  if (gmaj_stat == GSS_S_COMPLETE)
  {
  #ifdef APACHE13
  //AMX: repl by NS_Log ap_log_rerror (file, line, level, r, "%s; GSS-API mechanism: %s)", s, (char *) msg.value);
  #else
  //AMX: repl by NS_Log ap_log_rerror (file, line, level, 0, r, "%s; GSS-API mechanism: %s)", s, (char *) msg.value);
  #endif
  Ns_Log(Notice, "%s: Loaded private key file: %s", hModule, privatekeyfile);
  gss_release_buffer (&gmin_stat, &msg);
  break;
  }

  gss_release_buffer (&gmin_stat, &msg);
  }
  }
*/
/* -----------------------------------------------------------------------------
 * handlePoolCleanup handles pool cleanup.
 *
 * Is registered by calling apr_pool_cleanup_register. Is called by Apache.
 *
 * Returns an Apache status code.
 * -----------------------------------------------------------------------------
 */
/*
  #ifndef APACHE13
  static apr_status_t handlePoolCleanup (void * data)
  {
  connectionUser = NULL;
  return APR_SUCCESS;
  }
  #endif
*/
/* -----------------------------------------------------------------------------
 * handleKerberosToken handles an RFC 1964 Kerberos GSS-API token.
 *
 * Returns an Apache response code.
 * -----------------------------------------------------------------------------
 */

/*
 * Local Variables:
 * mode: c
 * c-basic-offset: 4
 * fill-column: 78
 * indent-tabs-mode: nil
 * End:
 */
