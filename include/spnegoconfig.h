/* -----------------------------------------------------------------------------
 * (c) Copyright 2008 KnowNow, Inc., Sunnyvale CA
 *
 * @KNOWNOW_LICENSE_START@
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in
 * the documentation and/or other materials provided with the
 * distribution.
 *
 * 3. The name "KnowNow" is a trademark of KnowNow, Inc. and may not
 * be used to endorse or promote any product without prior written
 * permission from KnowNow, Inc.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL KNOWNOW, INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @KNOWNOW_LICENSE_END@
 *
 * spnegoconfig.h declares classes for handling configuration parameters.
 *
 * Author: Avet Mnatsakanian
 *
 * -----------------------------------------------------------------------------
 */

#ifndef SPNEGOCONFIG_H
#define SPNEGOCONFIG_H

#define APR_HAS_ANSI_FS      1
#define APR_HAS_UNICODE_FS   0
#define APR_SUCCESS          0

#define HTTP_CONTINUE                      100
#define HTTP_SWITCHING_PROTOCOLS           101
#define HTTP_PROCESSING                    102
#define HTTP_OK                            200
#define HTTP_CREATED                       201
#define HTTP_ACCEPTED                      202
#define HTTP_NON_AUTHORITATIVE             203
#define HTTP_NO_CONTENT                    204
#define HTTP_RESET_CONTENT                 205
#define HTTP_PARTIAL_CONTENT               206
#define HTTP_MULTI_STATUS                  207
#define HTTP_MULTIPLE_CHOICES              300
#define HTTP_MOVED_PERMANENTLY             301
#define HTTP_MOVED_TEMPORARILY             302
#define HTTP_SEE_OTHER                     303
#define HTTP_NOT_MODIFIED                  304
#define HTTP_USE_PROXY                     305
#define HTTP_TEMPORARY_REDIRECT            307
#define HTTP_BAD_REQUEST                   400
#define HTTP_UNAUTHORIZED                  401
#define HTTP_PAYMENT_REQUIRED              402
#define HTTP_FORBIDDEN                     403
#define HTTP_NOT_FOUND                     404
#define HTTP_METHOD_NOT_ALLOWED            405
#define HTTP_NOT_ACCEPTABLE                406
#define HTTP_PROXY_AUTHENTICATION_REQUIRED 407
#define HTTP_REQUEST_TIME_OUT              408
#define HTTP_CONFLICT                      409
#define HTTP_GONE                          410
#define HTTP_LENGTH_REQUIRED               411
#define HTTP_PRECONDITION_FAILED           412
#define HTTP_REQUEST_ENTITY_TOO_LARGE      413
#define HTTP_REQUEST_URI_TOO_LARGE         414
#define HTTP_UNSUPPORTED_MEDIA_TYPE        415
#define HTTP_RANGE_NOT_SATISFIABLE         416
#define HTTP_EXPECTATION_FAILED            417
#define HTTP_UNPROCESSABLE_ENTITY          422
#define HTTP_LOCKED                        423
#define HTTP_FAILED_DEPENDENCY             424
#define HTTP_UPGRADE_REQUIRED              426
#define HTTP_INTERNAL_SERVER_ERROR         500
#define HTTP_NOT_IMPLEMENTED               501
#define HTTP_BAD_GATEWAY                   502
#define HTTP_SERVICE_UNAVAILABLE           503
#define HTTP_GATEWAY_TIME_OUT              504
#define HTTP_VERSION_NOT_SUPPORTED         505
#define HTTP_VARIANT_ALSO_VARIES           506
#define HTTP_INSUFFICIENT_STORAGE          507
#define HTTP_NOT_EXTENDED                  510
/*#ifdef __cplusplus
extern "C" {
#endif
*/
#include "ns.h"
#include <openssl/asn1.h>

/* Object identifier declarations. */

extern const ASN1_OBJECT spnegoGssApi; /* 1.3.6.1.5.5.2 */
/* -----------------------------------------------------------------------------
 * makeNegTokenTarg makes an RFC 2478 SPNEGO NegTokenTarg (token) from an
 * RFC 1964 Kerberos GSS-API token.
 *
 * If makeNegTokenTarg is successful, call free (*negTokenTarg) to free the
 * memory allocated by parseNegTokenInit.
 *
 * Returns 0 if successful, 1 otherwise.
 * -----------------------------------------------------------------------------
 */

class DIRECTORY_CONFIG
{
private:
    const char * krb5KeyTabFile;
    const char * krb5ServiceName;
public:
	void setKrb5KeyTabFile(char *tabFile){krb5KeyTabFile = tabFile;}
	void setKrb5ServiceName(char *srvcName){krb5ServiceName = srvcName;}
	const char* getKrb5KeyTabFile(){return krb5KeyTabFile;}
	const char* getKrb5ServiceName(){return krb5ServiceName;}
}; // DIRECTORY_CONFIG;


class SERVER_CONFIG
{
    int krb5AuthEachReq; 
public:
	void setKrb5AuthEachReq(int on){krb5AuthEachReq = on;}
	int getKrb5AuthEachReq(){return krb5AuthEachReq;}
}; 


class SPNEGO_CONFIG
{
public:
	static DIRECTORY_CONFIG *directoryConfig ;
	static SERVER_CONFIG *serverConfig;
	static const char *authType;
	static void setAuthType(char *type) {authType = type;}
	static const char *getAuthType() {return authType;}
	static DIRECTORY_CONFIG* getDirConfig(){return directoryConfig;};
	static SERVER_CONFIG* getServerConfig(){return serverConfig;};

}; 
int apr_env_set(const char *envvar,
                const char *value);
//int authenticateUser (Conn  *connPtr, const char *authToken);
/*
#ifdef __cplusplus
}
#endif
*/
#endif /*SPNEGOCONFIG_H */
