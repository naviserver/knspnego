/* -----------------------------------------------------------------------------
 * spnegohelp.h declares RFC 2478 SPNEGO GSS-API mechanism helper APIs.
 *
 * Author: Frank Balluffi
 *
 * Copyright (C) 2002-2005 Frank Balluffi. All rights reserved.
 * -----------------------------------------------------------------------------
 */

#ifndef SPNEGOHELP_H
#define SPNEGOHELP_H

#ifdef __cplusplus
extern "C" {
#endif


#include <openssl/asn1.h>

//#include <stddef.h>

/* Object identifier declarations. */

extern const ASN1_OBJECT spnegoGssApi; /* 1.3.6.1.5.5.2 */

/* -----------------------------------------------------------------------------
 * makeSpnegoInitialToken makes an RFC 2743 GSS-API InitialContextToken
 * containing an RFC 2478 SPNEGO NegotiationToken of choice negTokenInit from a
 * mechanism token.
 *
 * If makeSpnegoInitialToken is successful, call free (spnegoToken), where
 * spnegoToken is of type unsigned char *, to free the memory allocated by
 * makeSpnegoInitialToken.
 *
 * Returns 1 if successful, 0 otherwise.
 * -----------------------------------------------------------------------------
 */

int makeSpnegoInitialToken (const ASN1_OBJECT *   mechType,
                            const unsigned char * mechToken,
                            size_t                mechTokenLength,
                            unsigned char **      spnegoToken,
                            size_t *              spnegoTokenLength);

/* -----------------------------------------------------------------------------
 * makeSpnegoTargetToken makes an RFC 2743 GSS-API InitialContextToken
 * containing an RFC 2478 SPNEGO NegotiationToken of choice negTokenTarg from a
 * response token.
 *
 * If makeSpnegoTargetToken is successful, call free (spnegoToken), where
 * spnegoToken is of type unsigned char *, to free the memory allocated by
 * makeSpnegoTargetToken.
 *
 * Returns 1 if successful, 0 otherwise.
 * -----------------------------------------------------------------------------
 */

int makeSpnegoTargetToken (const long *          negResult,
                           const ASN1_OBJECT *   supportedMech,
                           const unsigned char * responseToken,
                           size_t                responseTokenLength,
                           const unsigned char * mechListMic,
                           size_t                mechListMicLength,
                           unsigned char **      spnegoToken,
                           size_t *              spnegoTokenLength);

/* -----------------------------------------------------------------------------
 * parseSpnegoInitialToken parses an RFC 2743 GSS-API InitialContextToken
 * containing an RFC 2478 SPNEGO NegotiationToken of choice negTokenInit, and
 * optionally searches for a mechanism type and extracts the mechanism token.
 *
 * If parseSpnegoInitialToken is successful, call free (mechToken), where
 * mechToken is of type unsigned char *, to free the memory allocated by
 * parseSpnegoInitialToken.
 *
 * Returns 1 if successful, 0 otherwise.
 * -----------------------------------------------------------------------------
 */

int parseSpnegoInitialToken (const unsigned char * spnegoToken,
                             size_t                spnegoTokenLength,
                             const ASN1_OBJECT *   mechType,
                             unsigned char **      mechToken,
                             size_t *              mechTokenLength);

/* -----------------------------------------------------------------------------
 * parseSpnegoTargetToken parses an RFC 2743 GSS-API InitialContextToken
 * containing an RFC 2478 SPNEGO NegotiationToken of choice negTokenTarg.
 *
 * If parseSpnegoTargetToken is successful, call
 * ASN1_OBJECT_free (supportedMech), free (responseToken) and
 * free (mechListMIC), where supportedMech, responseToken and mechListMIC are of
 * types ASN1_OBJECT *, unsigned char * and unsigned char *, to free the memory
 * allocated by parseSpnegoTargetToken.
 *
 * parseSpnegoTargetToken 1 if successful, 0 otherwise.
 * -----------------------------------------------------------------------------
 */

int parseSpnegoTargetToken (const unsigned char * spnegoToken,
                            size_t                spnegoTokenLength,
                            long *                negResult,
                            ASN1_OBJECT **        supportedMech,
                            unsigned char **      responseToken,
                            size_t *              responseTokenLength,
                            unsigned char **      mechListMIC,
                            size_t *              mechListMICLength);

#ifdef __cplusplus
}
#endif

#endif /* SPNEGOHELP_H */
