
ifndef NAVISERVER
    NAVISERVER  = /usr/local/ns
endif

ifndef FBOPENSSL
    FBOPENSSL=/usr/local/src/fbopenssl
endif

#
# Library definitions
#
MOD     = knspnego.so

MODOBJS = src/decode.o \
	src/knspnego.o \
	src/mod_spnego.o \
	src/win32-env.o \
	src/unix-env.o

MODLIBS=-lgssapi_krb5 -lfbopenssl -lcrypto
CPPFLAGS += -fPIC -DKN_PRIVATE=l -I$(NAVISERVER)/include -I$(FBOPENSSL)/include -I./include -I/usr/include/gssapi

include  $(NAVISERVER)/include/Makefile.module

#
# Force C++ linkage.  Use ':=' to invoke immediate evaluation.
#
LDLIB := $(subst gcc,g++,$(LDLIB))

install: install-knspnego

install-knspnego:
	$(INSTALL_SH) tcl/kn_spnego.tcl $(NAVISERVER)/modules/tcl/
	#$(INSTALL_SH) -d $(NAVISERVER)/bin winlib/i386/comerr32.dll
	#$(INSTALL_SH) -d $(NAVISERVER)/bin winlib/i386/gssapi32.dll
	#$(INSTALL_SH) -d $(NAVISERVER)/bin winlib/i386/krb5_32.dll

